package com.backendMobile.backendMobile.Entities;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ReservarRepository extends JpaRepository<Reservar, Integer>, JpaSpecificationExecutor<Reservar> {

	List<Reservar> findByUsuarioIdusuario(int idusuario);

	List<Reservar> findByIdPratoDia(Integer id);

}
