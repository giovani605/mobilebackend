package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "favorita")
@Entity
public class Favorita implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idfavorita")
    private Integer idfavorita;

    @Column(name = "usuario_idusuario")
    private Integer usuarioIdusuario;

    @Column(name = "restaurante_idrestaurante")
    private Integer restauranteIdrestaurante;

    public Integer getIdfavorita() {
        return idfavorita;
    }

    public void setIdfavorita(Integer idfavorita) {
        this.idfavorita = idfavorita;
    }

    public Integer getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Integer usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    public Integer getRestauranteIdrestaurante() {
        return restauranteIdrestaurante;
    }

    public void setRestauranteIdrestaurante(Integer restauranteIdrestaurante) {
        this.restauranteIdrestaurante = restauranteIdrestaurante;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
