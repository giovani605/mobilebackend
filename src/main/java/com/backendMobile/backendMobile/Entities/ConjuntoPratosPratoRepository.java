package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ConjuntoPratosPratoRepository extends JpaRepository<ConjuntoPratosPrato, Integer>, JpaSpecificationExecutor<ConjuntoPratosPrato> {

    List<ConjuntoPratosPrato> findByIdconjuntopratos(int idconjunto);

}
