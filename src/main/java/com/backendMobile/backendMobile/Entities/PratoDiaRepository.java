package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface PratoDiaRepository extends JpaRepository<PratoDia, Integer>, JpaSpecificationExecutor<PratoDia> {

    List<PratoDia> findAllByDataBetweenOrderByPrecoAsc(Date inicio, Date fim);

    @Query(value = "select p from PratoDia p where data >= ?1 and data <= ?2")
    List<PratoDia> queryPreco(Date inicio, Date fim);

    @Query(value = "select * from prato_dia where data >= ?1 and data <= ?2 order by preco", nativeQuery = true)
    List<PratoDia> queryNativa(Date inicio, Date fim);


    @Query(value = "select pd.* from prato_dia  pd inner join conjunto_pratos as con on con.id = pd.id_conjunto_pratos " +
            " inner join restaurante as r on r.idrestaurante = con.idrestaurante inner join "
            + " (select res.idrestaurante,sqrt(power(res.latitude - ?1,2) + power(res.longitude - ?2 ,2))"
            + " as distancia " +
            " from restaurante as res group by res.idrestaurante) as res on res.idrestaurante = r.idrestaurante "
            + "where pd.data >= ?3 and pd.data <= ?4 order by res.distancia", nativeQuery = true)
    List<PratoDia> queryDistancia(double lat, double longitude, Date inicio, Date fim);

	List<PratoDia> findByIdConjuntoPratos(Integer id);

    //List<PratoDia> findAllGreaterThanEqualDataLessThanEqualData(Date inicio, Date fim);
}

