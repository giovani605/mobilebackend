package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NotificacoesRepository extends JpaRepository<Notificacoes, Integer>, JpaSpecificationExecutor<Notificacoes> {

}