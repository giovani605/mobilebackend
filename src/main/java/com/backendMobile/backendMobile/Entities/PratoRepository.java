package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PratoRepository extends JpaRepository<Prato, Integer>, JpaSpecificationExecutor<Prato> {

    List<Prato> findByRestauranteIdrestaurante(int id);

    List<Prato> findByRestauranteIdrestauranteAndCategoriaIdcategoria(int id, int idcategoria);

    Prato findByIdpratos(int id);
}
