package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RestauranteRepository extends JpaRepository<Restaurante, Integer>, JpaSpecificationExecutor<Restaurante> {

    List<Restaurante> findById(int id);

    @Query(value = "select r.* from restaurante as r "
            + "inner join gerente as g on g.idgerente = r.gerente_idgerente "
            + "inner join usuario u "
            + "on g.usuario_idusuario =  u.idusuario where u.idusuario = ?1 ", nativeQuery = true)
    Restaurante carregarRestauranteGerente(int idusuario);

    Restaurante findByIdrestaurante(int id);

    @Query(value = "select r.* from favorita as f inner join restaurante as r on r.idrestaurante = f.restaurante_idrestaurante where f.usuario_idusuario = ?1", nativeQuery = true)
    List<Restaurante> query(int idusuario);


}