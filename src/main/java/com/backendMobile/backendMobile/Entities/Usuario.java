package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "usuario")
@Data
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idusuario", insertable = false, nullable = false)
    private Integer idusuario;

    @Column(name = "nome")
    private String nome;

    @Column(name = "CPF")
    private String CPF;

    @Column(name = "senha")
    private String senha;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "email")
    private String email;

    @Column(name = "ativado")
    private Integer ativado = 1;

    @Column(name = "data_hora_cadastro")
    private Date dataHoraCadastro;

    @Column(name = "autentificacao")
    private String autentificacao;

	public Integer getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCPF() {
		return CPF;
	}

	public void setCPF(String cPF) {
		CPF = cPF;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAtivado() {
		return ativado;
	}

	public void setAtivado(Integer ativado) {
		this.ativado = ativado;
	}

	public Date getDataHoraCadastro() {
		return dataHoraCadastro;
	}

	public void setDataHoraCadastro(Date dataHoraCadastro) {
		this.dataHoraCadastro = dataHoraCadastro;
	}

	public String getAutentificacao() {
		return autentificacao;
	}

	public void setAutentificacao(String autentificacao) {
		this.autentificacao = autentificacao;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

    
    



}