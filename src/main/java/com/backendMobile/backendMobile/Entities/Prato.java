package com.backendMobile.backendMobile.Entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

@Entity
@Table(name = "prato")
@Data
public class Prato implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpratos", insertable = false, nullable = false)
    private Integer idpratos;

    @Column(name = "restaurante_idrestaurante", nullable = false)
    private Integer restauranteIdrestaurante;

    @Column(name = "idimagem", nullable = false)
    private String idimagem;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "categoria_idcategoria", nullable = false)
    private Integer categoriaIdcategoria;

    @Transient
    private Categoria categoria;

    @JsonProperty("categoria")
    private void unpackNested(Map<String, Object> brand) {
        ObjectMapper objectMapper = new ObjectMapper();
        if (brand == null) {
            return;
        }

        try {
            this.categoria = objectMapper.readValue(objectMapper.writeValueAsBytes(brand), Categoria.class);
            this.categoriaIdcategoria = this.categoria.getIdCategoria();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Integer getCategoriaIdCategoria() {
        return categoriaIdcategoria;
    }

    public void setCategoriaIdCategoria(Integer categoriaIdCategoria) {
        this.categoriaIdcategoria = categoriaIdCategoria;
    }

    public Integer getIdpratos() {
        return idpratos;
    }

    public void setIdpratos(Integer idpratos) {
        this.idpratos = idpratos;
    }

    public Integer getRestauranteIdrestaurante() {
        return restauranteIdrestaurante;
    }

    public void setRestauranteIdrestaurante(Integer restauranteIdrestaurante) {
        this.restauranteIdrestaurante = restauranteIdrestaurante;
    }

    public String getIdimagem() {
        return idimagem;
    }

    public void setIdimagem(String idimagem) {
        this.idimagem = idimagem;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
