package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "conjunto_pratos_prato")
@Data
public class ConjuntoPratosPrato implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtabela")
    private Integer idtabela;

    @Column(name = "idconjuntopratos")
    private Integer idconjuntopratos;

    @Column(name = "idpratochave")
    private Integer idpratochave;


    public Integer getIdtabela() {
        return idtabela;
    }

    public void setIdtabela(Integer idtabela) {
        this.idtabela = idtabela;
    }

    public Integer getIdconjuntopratos() {
        return idconjuntopratos;
    }

    public void setIdconjuntopratos(Integer idconjuntopratos) {
        this.idconjuntopratos = idconjuntopratos;
    }

    public Integer getIdpratochave() {
        return idpratochave;
    }

    public void setIdpratochave(Integer idpratochave) {
        this.idpratochave = idpratochave;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }


}
