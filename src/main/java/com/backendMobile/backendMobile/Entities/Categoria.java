package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "categoriapratos")
@Data
public class Categoria implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "idCategoria", nullable = false)
    private Integer idcategoria;

    @Column(name = "categoria", nullable = false)
    private String categoria;

    @Column(name = "abreviacao")
    private String abreviacao;

//	@OneToMany
//	@JoinColumn(name="Categoria_idCategoria", nullable=false)
//	List<Prato> pratos;

    public Integer getIdCategoria() {
        return idcategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idcategoria = idCategoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
