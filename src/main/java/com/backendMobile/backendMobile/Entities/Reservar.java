package com.backendMobile.backendMobile.Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "reservar")
@Entity
public class Reservar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idreserva",insertable = true, nullable = false)
	private Integer idreserva;

	@Column(name = "usuario_idusuario")
	private Integer usuarioIdusuario;

	@Column(name = "id_prato_dia")
	private Integer idPratoDia;

	@Column(name = "observacao")
	private String observacao;

	public Integer getIdreserva() {
		return idreserva;
	}

	public void setIdreserva(Integer idreserva) {
		this.idreserva = idreserva;
	}

	public Integer getUsuarioIdusuario() {
		return usuarioIdusuario;
	}

	public void setUsuarioIdusuario(Integer usuarioIdusuario) {
		this.usuarioIdusuario = usuarioIdusuario;
	}

	public Integer getIdPratoDia() {
		return idPratoDia;
	}

	public void setIdPratoDia(Integer idPratoDia) {
		this.idPratoDia = idPratoDia;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

}
