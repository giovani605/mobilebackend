package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ConjuntoPratosRepository extends JpaRepository<ConjuntoPratos, Integer>, JpaSpecificationExecutor<ConjuntoPratos> {
    List<ConjuntoPratos> findByIdrestaurante(int id);

    ConjuntoPratos findById(int id);

}
