package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface FavoritaRepository extends JpaRepository<Favorita, Integer>, JpaSpecificationExecutor<Favorita> {


}

