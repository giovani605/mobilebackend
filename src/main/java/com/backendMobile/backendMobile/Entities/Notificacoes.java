package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "notificacoes")
@Entity
public class Notificacoes implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "idnotificacoes", nullable = false)
    private Integer idnotificacoes;

    @Column(name = "idusuario", nullable = false)
    private Integer idusuario;

    @Column(name = "ativo", nullable = false)
    private Integer ativo;

    @Column(name = "descricao")
    private String descricao;


}