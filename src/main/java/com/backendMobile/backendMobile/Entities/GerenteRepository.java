package com.backendMobile.backendMobile.Entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GerenteRepository extends JpaRepository<Gerente, Integer>, JpaSpecificationExecutor<Gerente> {

}
