package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "conjunto_pratos")
@Data
@Entity
public class ConjuntoPratos implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "nome")
    private String nome;

    @Column(name = "idprincipal")
    private int idprincipal;

    @Column(name = "idrestaurante")
    private int idrestaurante;


    public int getIdrestaurante() {
        return idrestaurante;
    }

    public void setIdrestaurante(int idrestaurante) {
        this.idrestaurante = idrestaurante;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdprincipal() {
        return idprincipal;
    }

    public void setIdprincipal(int idprincipal) {
        this.idprincipal = idprincipal;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
