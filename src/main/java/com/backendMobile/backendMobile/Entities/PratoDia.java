package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "prato_dia")
@Entity
@Data
public class PratoDia implements Serializable {
    private static final long serialVersionUID = 1L;

    public PratoDia() {

    }

    public PratoDia(Object[] array) {
        System.out.println("a");


        // TODO Auto-generated constructor stub
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, nullable = false)
    private Integer id;

    @Column(name = "data", nullable = false)
    private Date data;

    @Column(name = "id_conjunto_pratos", nullable = false)
    private Integer idConjuntoPratos;

    @Column(name = "preco")
    private Double preco;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getIdConjuntoPratos() {
        return idConjuntoPratos;
    }

    public void setIdConjuntoPratos(Integer idConjuntoPratos) {
        this.idConjuntoPratos = idConjuntoPratos;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
