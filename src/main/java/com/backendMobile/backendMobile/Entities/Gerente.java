package com.backendMobile.backendMobile.Entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "gerente")
public class Gerente implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idgerente", insertable = false, nullable = false)
    private Integer idgerente;

    @Column(name = "usuario_idusuario", nullable = false)
    private Integer usuarioIdusuario;

    public Integer getIdgerente() {
        return idgerente;
    }

    public void setIdgerente(Integer idgerente) {
        this.idgerente = idgerente;
    }

    public Integer getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Integer usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}