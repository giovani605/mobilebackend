package com.backendMobile.backendMobile.POJO;
import java.util.List;

import com.backendMobile.backendMobile.Entities.ConjuntoPratos;
import com.backendMobile.backendMobile.Entities.Prato;
import com.backendMobile.backendMobile.Entities.PratoDia;
import com.backendMobile.backendMobile.Entities.Reservar;
import com.backendMobile.backendMobile.Entities.Restaurante;
import com.backendMobile.backendMobile.Entities.Usuario;

public class ReservaPOJO {
	
	private Restaurante restaurante;
	private PratoDia pratodis;
	private List<Prato> listaPratos;
	private Reservar reserva;
	private ConjuntoPratos conjunto;
	private Usuario user;
	public Restaurante getRestaurante() {
		return restaurante;
	}
	public void setRestaurante(Restaurante restaurante) {
		this.restaurante = restaurante;
	}
	public PratoDia getPratodis() {
		return pratodis;
	}
	public void setPratodis(PratoDia pratodis) {
		this.pratodis = pratodis;
	}
	public List<Prato> getListaPratos() {
		return listaPratos;
	}
	public void setListaPratos(List<Prato> listaPratos) {
		this.listaPratos = listaPratos;
	}
	public Reservar getReserva() {
		return reserva;
	}
	public void setReserva(Reservar reserva) {
		this.reserva = reserva;
	}
	public ConjuntoPratos getConjunto() {
		return conjunto;
	}
	public void setConjunto(ConjuntoPratos conjunto) {
		this.conjunto = conjunto;
	}
	public Usuario getUser() {
		return user;
	}
	public void setUser(Usuario user) {
		this.user = user;
	}
	
	

}
