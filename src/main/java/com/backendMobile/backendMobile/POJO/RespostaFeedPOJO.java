package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.PratoDia;

import java.util.ArrayList;
import java.util.List;

public class RespostaFeedPOJO {
    List<PratoDia> listaPratoDia;

    List<ItemFeed> listaFeed = new ArrayList<ItemFeed>();


    public List<ItemFeed> getListaFeed() {
        return listaFeed;
    }

    public void setListaFeed(List<ItemFeed> listaFeed) {
        this.listaFeed = listaFeed;
    }

    public List<PratoDia> getListaPratoDia() {
        return listaPratoDia;
    }

    public void setListaPratoDia(List<PratoDia> listaPratoDia) {
        this.listaPratoDia = listaPratoDia;
    }

    private String mensagem;

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    private boolean flag;

}
