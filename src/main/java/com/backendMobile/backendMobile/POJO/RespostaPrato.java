package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.Prato;

import java.util.List;

public class RespostaPrato {

    boolean flag;

    String mensagem;


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    List<Prato> listaPratos;
    List<Prato> listaPratosEntrada;

    List<Prato> listaPratosSobremesa;

    List<Prato> listaPratosPrincipal;

    public List<Prato> getListaPratos() {
        return listaPratos;
    }

    public void setListaPratos(List<Prato> listaPratos) {
        this.listaPratos = listaPratos;
    }

    public List<Prato> getListaPratosEntrada() {
        return listaPratosEntrada;
    }

    public void setListaPratosEntrada(List<Prato> listaPratosEntrada) {
        this.listaPratosEntrada = listaPratosEntrada;
    }

    public List<Prato> getListaPratosSobremesa() {
        return listaPratosSobremesa;
    }

    public void setListaPratosSobremesa(List<Prato> listaPratosSobremesa) {
        this.listaPratosSobremesa = listaPratosSobremesa;
    }

    public List<Prato> getListaPratosPrincipal() {
        return listaPratosPrincipal;
    }

    public void setListaPratosPrincipal(List<Prato> listaPratosPrincipal) {
        this.listaPratosPrincipal = listaPratosPrincipal;
    }


}
