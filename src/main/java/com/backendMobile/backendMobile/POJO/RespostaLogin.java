package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.Usuario;

public class RespostaLogin {

    private String mensagem;

    private boolean flag;

    private Usuario user;

    public RespostaLogin() {

    }


    public RespostaLogin(String mensagem, boolean flag, Usuario user) {
        super();
        this.mensagem = mensagem;
        this.flag = flag;
        this.user = user;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }


}
