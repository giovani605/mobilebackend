package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.ConjuntoPratos;

import java.util.List;

public class RespostaConjuntoPratos {
    private String mensagem;

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    private boolean flag;

    private List<ConjuntoPratos> lista;

    public List<ConjuntoPratos> getLista() {
        return lista;
    }

    public void setLista(List<ConjuntoPratos> lista) {
        this.lista = lista;
    }

}
