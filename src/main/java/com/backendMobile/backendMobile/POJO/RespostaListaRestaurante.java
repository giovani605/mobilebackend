package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.Restaurante;

import java.util.List;

public class RespostaListaRestaurante {
    private List<Restaurante> listaRestaurante;

    public List<Restaurante> getListaRestaurante() {
        return listaRestaurante;
    }

    public void setListaRestaurante(List<Restaurante> listaRestaurante) {
        this.listaRestaurante = listaRestaurante;
    }

    private String mensagem;

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    private boolean flag;


}
