package com.backendMobile.backendMobile.POJO;

import com.backendMobile.backendMobile.Entities.ConjuntoPratos;
import com.backendMobile.backendMobile.Entities.Prato;
import com.backendMobile.backendMobile.Entities.PratoDia;
import com.backendMobile.backendMobile.Entities.Restaurante;

public class ItemFeed {
    private PratoDia pratodia;

    private Restaurante restaurante;

    private ConjuntoPratos conjunto;

    private Prato pratoPrincipal;

    public PratoDia getPratodia() {
        return pratodia;
    }

    public void setPratodia(PratoDia pratodia) {
        this.pratodia = pratodia;
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public ConjuntoPratos getConjunto() {
        return conjunto;
    }

    public void setConjunto(ConjuntoPratos conjunto) {
        this.conjunto = conjunto;
    }

    public Prato getPratoPrincipal() {
        return pratoPrincipal;
    }

    public void setPratoPrincipal(Prato pratoPrincipal) {
        this.pratoPrincipal = pratoPrincipal;
    }


}
