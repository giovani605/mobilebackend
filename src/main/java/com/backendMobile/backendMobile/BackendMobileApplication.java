package com.backendMobile.backendMobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendMobileApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendMobileApplication.class, args);
    }

//	@Bean
//    CommandLineRunner init(StorageService  storageService) {
//        return (args) -> {
//            // storageService.deleteAll();
//            storageService.init();
//        };
//    }

}
