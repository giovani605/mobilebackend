package com.backendMobile.backendMobile.Controllers;

import com.backendMobile.backendMobile.Entities.*;
import com.backendMobile.backendMobile.POJO.RespostaListaRestaurante;
import com.backendMobile.backendMobile.POJO.RespostaPadraoPOJO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/restaurante")
public class RestauranteController {

    private final RestauranteRepository restauranteRepository;

    @Autowired
    private EmailController emailController;

    public RestauranteController(RestauranteRepository restauranteRepository) {
        this.restauranteRepository = restauranteRepository;
    }

    /**
     * Função REST que retorna toda a lista de restaurantes
     *
     * @return retorna uma lista de restaurantes ou not found
     */
    @SuppressWarnings("ConstantConditions")
    @GetMapping
    public ResponseEntity<List<Restaurante>> getRestaurante() {
        List<Restaurante> listaRestaurantes = this.restauranteRepository.findAll();

        if (listaRestaurantes == null || (listaRestaurantes != null && listaRestaurantes.size() == 0)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(listaRestaurantes);
    }

    /**
     * Retorna um restaurante especifico
     *
     * @param id identificador do restaurante
     * @return retorna o restaurante ou not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<Restaurante> getRestaurante(@PathVariable Integer id) {
        Restaurante restaurante = this.restauranteRepository.getOne(id);

        if (restaurante == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(restaurante);
    }

    /**
     * Adiciona um novo restaurante ao banco de dados
     *
     * @param restaurante novo restaurante
     * @return retorno o novo restaurante salvo
     */

    @Autowired
    GerenteRepository gerenteRepository;

    @Autowired
    UsuarioRepository usuarioRepository;


    @PostMapping
    public RespostaPadraoPOJO cadastrarRestaurante(@RequestBody Map<String, Object> req) {

        ObjectMapper objectMapper = new ObjectMapper();
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        try {
            Usuario user = objectMapper.readValue(req.get("user").toString(), Usuario.class);
            Restaurante restaurante = objectMapper.readValue(req.get("restaurante").toString(), Restaurante.class);
            user.setAtivado(1);
            user.setDataHoraCadastro(new Date());
            Usuario userSalvo = usuarioRepository.save(user);

            String codigo = this.emailController.enviarAutentificacao(userSalvo.getIdusuario(), userSalvo.getNome(), userSalvo.getEmail());
            if (codigo != null) {
                userSalvo.setAutentificacao(codigo);
                userSalvo = this.usuarioRepository.save(userSalvo);
            } else {
                this.usuarioRepository.delete(userSalvo);
                new Exception();
            }


            Gerente gerente = new Gerente();
            gerente.setUsuarioIdusuario(userSalvo.getIdusuario());
            Gerente gerenteSalvo = gerenteRepository.save(gerente);

            restaurante.setGerenteIdgerente(gerenteSalvo.getIdgerente());
            Restaurante restauranteSalvo = restauranteRepository.save(restaurante);

            // Restaurante r = this.restauranteRepository.save(req.restaurante);
            resposta.setFlag(true);
            resposta.setMensagem("Restaurante Salvo com sucesso");
        } catch (Exception e) {
            resposta.setFlag(false);
            resposta.setMensagem("Falha");
        }
        return resposta;

    }

    /**
     * Altera um restaurante já existente
     *
     * @param id          identifdicador do restaurante que se deseja alterar
     * @param restaurante novas informações do restaurante
     * @return retorno o restaurante atualizado
     */
    @PutMapping("/{id}")
    public ResponseEntity<Restaurante> setRestaurante(@PathVariable Integer id,
                                                      @Valid @RequestBody Restaurante restaurante) {
        Restaurante existente = this.restauranteRepository.getOne(id);

        if (existente == null) {
            return ResponseEntity.notFound().build();
        }

        BeanUtils.copyProperties(restaurante, existente, "id");

        existente = this.restauranteRepository.save(existente);

        return ResponseEntity.ok(existente);
    }


    @GetMapping("/administracao/{id}")
    public ResponseEntity<Restaurante> getRestaurante(@PathVariable int id) {
        Restaurante restaurante = this.restauranteRepository.carregarRestauranteGerente(id);

        if (restaurante == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(restaurante);
    }

    @Autowired
    FavoritaRepository favoritaRepository;


    @PostMapping("/favoritar/{idRestaurante}/{idUsuario}")
    public RespostaPadraoPOJO resgistrarFavorito(@PathVariable("idRestaurante") int idRestaurante, @PathVariable("idUsuario") int idusuario) {
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        Favorita favorita = new Favorita();
        favorita.setRestauranteIdrestaurante(idRestaurante);
        favorita.setUsuarioIdusuario(idusuario);
        try {
            favoritaRepository.save(favorita);
            resposta.setFlag(true);
            resposta.setMensagem("TUDO OK");
        } catch (Exception e) {
            resposta.setFlag(false);
            resposta.setMensagem("TUDO ERRADO");
        }

        return resposta;

    }

    @GetMapping("/favorito/{idUsuario}")
    public RespostaListaRestaurante resgistrarFavorito(@PathVariable("idUsuario") int idusuario) {
        RespostaListaRestaurante resposta = new RespostaListaRestaurante();
        List<Restaurante> lista = restauranteRepository.query(idusuario);
        resposta.setFlag(true);
        resposta.setMensagem("TOP");
        resposta.setListaRestaurante(lista);

        return resposta;

    }


}