package com.backendMobile.backendMobile.Controllers;

import com.backendMobile.backendMobile.Entities.*;
import com.backendMobile.backendMobile.POJO.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

@RestController
@RequestMapping("/prato")
public class PratoController {

    @Autowired
    PratoRepository pratoRepo;

    @Autowired
    CategoriaRepository categoriaController;

    @Autowired
    ConjuntoPratosRepository conjuntoRepository;

    @Autowired
    ConjuntoPratosPratoRepository conjuntopratospratosRepository;

    @GetMapping("/restaurante/{id}")
    public RespostaPrato recuperarPratosRestaurante(@PathVariable("id") int id) {
        RespostaPrato respostaPrato = new RespostaPrato();
        List<Prato> listapratos = pratoRepo.findByRestauranteIdrestaurante(id);
        respostaPrato.setListaPratos(listapratos);

        // Carregar os tipos de prato separados
        List<Prato> listapratosEntrada = pratoRepo.findByRestauranteIdrestauranteAndCategoriaIdcategoria(id, 2);
        List<Prato> listapratosPrincipal = pratoRepo.findByRestauranteIdrestauranteAndCategoriaIdcategoria(id, 1);
        List<Prato> listapratosSobremesa = pratoRepo.findByRestauranteIdrestauranteAndCategoriaIdcategoria(id, 3);

        respostaPrato.setListaPratosEntrada(listapratosEntrada);
        respostaPrato.setListaPratosPrincipal(listapratosPrincipal);
        respostaPrato.setListaPratosSobremesa(listapratosSobremesa);

        respostaPrato.setFlag(true);
        respostaPrato.setMensagem("ok");

        return respostaPrato;
    }

    @GetMapping("/conjunto/{id}")
    public RespostaConjuntoPratos recuperarConjutoPratosRestaurante(@PathVariable("id") int id) {
        RespostaConjuntoPratos respostaConjuntoPratos = new RespostaConjuntoPratos();

        List<ConjuntoPratos> lista = conjuntoRepository.findByIdrestaurante(id);
        respostaConjuntoPratos.setLista(lista);

        respostaConjuntoPratos.setFlag(true);
        respostaConjuntoPratos.setMensagem("ok");

        return respostaConjuntoPratos;
    }

    @GetMapping("/feed/conjunto/{id}")
    public RespostaPrato recuperarpratosPorConjunto(@PathVariable("id") int idconjunto) {
        RespostaPrato resposta = new RespostaPrato();
        List<Prato> listapratos = new ArrayList<Prato>();
        List<ConjuntoPratosPrato> con = conjuntopratospratosRepository.findByIdconjuntopratos(idconjunto);
        for (ConjuntoPratosPrato conjunto : con) {
            Prato p = pratoRepo.findByIdpratos(conjunto.getIdpratochave());
            listapratos.add(p);


        }
        resposta.setListaPratos(listapratos);
        return resposta;

    }

    @Autowired
    PratoDiaRepository pratodiaRepo;

    @PostMapping("/dia")
    public RespostaPadraoPOJO cadastrarPratoDia(@RequestBody Map<String, Object> mapa) {
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        ObjectMapper objectMapper = new ObjectMapper();
        PratoDia pratodia = new PratoDia();
        try {
            pratodia.setPreco(Double.valueOf(mapa.get("preco").toString()));

            pratodia.setData(objectMapper.readValue(mapa.get("data").toString(), Date.class));

            pratodia.setIdConjuntoPratos(Integer.decode(mapa.get("idConjunto").toString()));

            pratodiaRepo.save(pratodia);
            resposta.setFlag(true);
            resposta.setMensagem("ok");

        } catch (Exception e) {
            System.out.println(e);
            resposta.setFlag(false);
            resposta.setMensagem("erro");
        }

        return resposta;
    }

    @PostMapping("/conjunto")
    public RespostaPadraoPOJO recuperarPratosRestaurante(@RequestBody Map<String, Object> mapa) {
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Restaurante r = objectMapper.readValue(mapa.get("restaurante").toString(), Restaurante.class);
            ConjuntoPratos conjunto = new ConjuntoPratos();
            conjunto.setDescricao(mapa.get("descricao").toString());
            conjunto.setNome(mapa.get("nome").toString());
            conjunto.setIdrestaurante(r.getIdrestaurante());

            Prato pratoprincipal = objectMapper.readValue(mapa.get("pratoPrincipal").toString(), Prato.class);
            Prato pratoEntrada = objectMapper.readValue(mapa.get("pratoEntrada").toString(), Prato.class);
            Prato pratoSobremesa = objectMapper.readValue(mapa.get("pratoSobremesa").toString(), Prato.class);

            conjunto.setIdprincipal(pratoprincipal.getIdpratos());

            ConjuntoPratos salvo = conjuntoRepository.save(conjunto);

            // salvar os outros pratos
            ConjuntoPratosPrato c1 = new ConjuntoPratosPrato();
            c1.setIdpratochave(pratoprincipal.getIdpratos());
            c1.setIdconjuntopratos(salvo.getId());
            conjuntopratospratosRepository.save(c1);

            ConjuntoPratosPrato c2 = new ConjuntoPratosPrato();
            c2.setIdpratochave(pratoEntrada.getIdpratos());
            c2.setIdconjuntopratos(salvo.getId());
            conjuntopratospratosRepository.save(c2);

            ConjuntoPratosPrato c3 = new ConjuntoPratosPrato();
            c3.setIdpratochave(pratoSobremesa.getIdpratos());
            c3.setIdconjuntopratos(salvo.getId());
            conjuntopratospratosRepository.save(c3);

            resposta.setFlag(true);
            resposta.setMensagem("ok");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            resposta.setFlag(false);
            resposta.setMensagem("erro");
        }
        return resposta;
    }

    @PostMapping("/cadastrar")
    public RespostaPadraoPOJO cadastrarPrato(@RequestBody Map<String, Object> mapa) {
        ObjectMapper objectMapper = new ObjectMapper();
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        try {

            // Extrarir os dados

            String dados = mapa.get("imagem").toString();
            byte[] arrayBytes = Base64.getDecoder().decode(dados);
            String uniqueID = UUID.randomUUID().toString();
            File f = new File("./images/" + uniqueID);
            OutputStream stream = new FileOutputStream(f);
            stream.write(arrayBytes);

            Prato prato = objectMapper.readValue(mapa.get("prato").toString(), Prato.class);
            prato.setIdimagem(uniqueID);
            pratoRepo.save(prato);

            // REgistrar e fechar

            resposta.setFlag(true);
            resposta.setMensagem("Prato Salvo com sucesso");
        } catch (Exception e) {
            resposta.setFlag(false);
            resposta.setMensagem("Falha");

        }
        return resposta;
    }

    @Autowired
    RestauranteRepository restauranteRepository;

    @GetMapping("/feed")
    public RespostaFeedPOJO gerarFeed() {

        RespostaFeedPOJO resposta = new RespostaFeedPOJO();
        Calendar cal1 = new GregorianCalendar();
        cal1.add(Calendar.DAY_OF_MONTH, -1);
        Date inicio = cal1.getTime();
        cal1.add(Calendar.DAY_OF_MONTH, 2);
        Date fim = cal1.getTime();
        List<PratoDia> listaPratodia = pratodiaRepo.findAllByDataBetweenOrderByPrecoAsc(inicio, fim);
        resposta.setListaPratoDia(listaPratodia);
        for (PratoDia pd : listaPratodia) {
            ItemFeed item = new ItemFeed();
            ConjuntoPratos conjunto = conjuntoRepository.findById(pd.getIdConjuntoPratos()).get();
            item.setConjunto(conjunto);
            item.setPratodia(pd);
            Prato p = pratoRepo.findByIdpratos(conjunto.getIdprincipal());
            item.setPratoPrincipal(p);

            Restaurante r = restauranteRepository.findByIdrestaurante(conjunto.getIdrestaurante());
            item.setRestaurante(r);
            resposta.getListaFeed().add(item);

        }

        resposta.setFlag(true);
        resposta.setMensagem("ok");

        return resposta;
    }

    @GetMapping("/feed/localidade")
    public RespostaFeedPOJO gerarFeedDistancia() {

        RespostaFeedPOJO resposta = new RespostaFeedPOJO();
        Calendar cal1 = new GregorianCalendar();
        cal1.add(Calendar.DAY_OF_MONTH, -1);
        Date inicio = cal1.getTime();
        cal1.add(Calendar.DAY_OF_MONTH, 2);
        Date fim = cal1.getTime();
        // List<PratoDia> listaPratodia =
        // pratodiaRepo.findAllByDataBetweenOrderByPreco(inicio, fim);
        double lat = -42.20;
        double longitude = -42.20;

        List<PratoDia> listaPratodia = pratodiaRepo.queryDistancia(lat, longitude, inicio, fim);
        resposta.setListaPratoDia(listaPratodia);
        for (PratoDia pd : listaPratodia) {
            ItemFeed item = new ItemFeed();
            ConjuntoPratos conjunto = conjuntoRepository.findById(pd.getIdConjuntoPratos()).get();
            item.setConjunto(conjunto);
            item.setPratodia(pd);
            Prato p = pratoRepo.findByIdpratos(conjunto.getIdprincipal());
            item.setPratoPrincipal(p);

            Restaurante r = restauranteRepository.findByIdrestaurante(conjunto.getIdrestaurante());
            item.setRestaurante(r);
            resposta.getListaFeed().add(item);

        }

        resposta.setFlag(true);
        resposta.setMensagem("ok");

        return resposta;
    }

		@Autowired
	ReservarRepository reservarRepository;
	
	@PostMapping("/reservar")
	public RespostaPadraoPOJO reservarPratodia(@RequestBody Map<String, Object> mapa) {
		RespostaPadraoPOJO respostaPadraoPOJO = new RespostaPadraoPOJO();
		
		try {
			Reservar reserva = new Reservar();
			ObjectMapper mapper =  new ObjectMapper();
			reserva.setObservacao(mapa.get("observacoes").toString());
			reserva.setUsuarioIdusuario(Integer.parseInt(mapa.get("idusuario").toString()));
			reserva.setIdPratoDia(Integer.parseInt(mapa.get("idpratodia").toString()));
			if(reserva.getObservacao() == null) {
				reserva.setObservacao("");
			}
			reservarRepository.save(reserva);
			
			
			respostaPadraoPOJO.setFlag(true);
			respostaPadraoPOJO.setMensagem("Certo");
		}catch (Exception e) {
			System.out.println(e);
			respostaPadraoPOJO.setFlag(false);
			respostaPadraoPOJO.setMensagem("problemas");
		}
		
		
		
		
		
		
		return respostaPadraoPOJO;
	}
	
	@Autowired
	ReservarRepository reservaRepository;
	
	@GetMapping("/reservas/cliente/{id}")
	public RespostaReservaCliente recuperarReservasClientes(@PathVariable("id") int idusuario) {
		RespostaReservaCliente resposta = new RespostaReservaCliente();
		try {
			// carregar as reservas do cliente
			List<Reservar> listaReservas = reservaRepository.findByUsuarioIdusuario(idusuario);
			List<ReservaPOJO> listapojo = new ArrayList<>();
			
			
			for(Reservar reserva : listaReservas) {
				ReservaPOJO pojo = new ReservaPOJO();
				pojo.setReserva(reserva);
				PratoDia pratodia = pratodiaRepo.findById(reserva.getIdPratoDia()).get();
				pojo.setPratodis(pratodia);
				ConjuntoPratos conjunto = conjuntoRepository.findById(pratodia.getIdConjuntoPratos()).get();
				pojo.setConjunto(conjunto);
				Restaurante r = restauranteRepository.findByIdrestaurante(conjunto.getIdrestaurante());
				pojo.setRestaurante(r);	
				listapojo.add(pojo);
			}
			resposta.setListaFavoritos(listapojo);
			resposta.setFlag(true);
			resposta.setMensagem("TUDO ok");
		}catch (Exception e) {
			resposta.setFlag(false);
			resposta.setMensagem("Erros");
		}
		
		
		
		
		return resposta;
		
	}
	
	@Autowired
	UsuarioRepository userRepository;
	
	@GetMapping("/reservas/restaurante/{id}")
	public RespostaReservaCliente recuperarReservasRestaurante(@PathVariable("id") int id) {
		RespostaReservaCliente resposta = new RespostaReservaCliente();
		try {
			ArrayList<ReservaPOJO> listaPojo = new ArrayList<ReservaPOJO>();
			// carregar as reservas do cliente
			Restaurante r = restauranteRepository.findByIdrestaurante(id);
			List<ConjuntoPratos> listaConjuntos = conjuntoRepository.findByIdrestaurante(id);
			for(ConjuntoPratos conjunto : listaConjuntos) {
				List<PratoDia> listaPratosDias = pratodiaRepo.findByIdConjuntoPratos(conjunto.getId());
				for(PratoDia pratoDia : listaPratosDias) {
					List<Reservar> listaReservas = reservarRepository.findByIdPratoDia(pratoDia.getId());
					for(Reservar reserva : listaReservas) {
						ReservaPOJO pojo = new ReservaPOJO();
						pojo.setRestaurante(r);
						pojo.setConjunto(conjunto);
						pojo.setPratodis(pratoDia);
						pojo.setReserva(reserva);
						Usuario user = userRepository.findById(reserva.getUsuarioIdusuario()).get();
						pojo.setUser(user);
						listaPojo.add(pojo);
					}
				}
				
				
			}
			
			resposta.setListaFavoritos(listaPojo);
			resposta.setFlag(true);
			resposta.setMensagem("TUDO ok");
		}catch (Exception e) {
			resposta.setFlag(false);
			resposta.setMensagem("Erros");
		}
		
		return resposta;
		
	}


}
