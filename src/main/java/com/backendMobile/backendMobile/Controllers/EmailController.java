package com.backendMobile.backendMobile.Controllers;

import com.backendMobile.backendMobile.Entities.Usuario;
import com.backendMobile.backendMobile.Entities.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

@RestController
@RequestMapping("/autentificacao")
public class EmailController {

    @Autowired
    private JavaMailSender disparadorEmails;

    @Autowired
    private UsuarioRepository usuarioRepository;

    // Carcteres possíveis para geração da string randomica
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static String ipBackEnd;

    static {
        try {
            ipBackEnd = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }


    private String gerarStringRandomica() {
        // Gerador de strings alpha númericos
        StringBuilder builder = new StringBuilder();

        for (int contador = 0; contador < 50; contador++) {
            builder.append(ALPHA_NUMERIC_STRING.charAt((int) (Math.random() * ALPHA_NUMERIC_STRING.length())));
        }

        return builder.toString();
    }

    protected String enviarAutentificacao(Integer id, String nome, String email) {
        SimpleMailMessage mensagem = new SimpleMailMessage();
        String stringRandomica = this.gerarStringRandomica();

        mensagem.setTo(email);
        mensagem.setSubject("Autentificação da conta uFAT");
        mensagem.setText(
                "Olá " + nome + " obrigado por se cadastrar no aplicativo uFAT, esperamos que goste da experiência.\n\n" +
                        "Clique no link abaixo para autentificar seu novo cadastro:\n" +
                        "http://" + this.ipBackEnd + ":8080/autentificacao" + "/autentificar/usuario/" + id + "/" + stringRandomica + "\n\n" +
                        "Caso a autenficação falhe, clique no link abaixo para enviar um novo código de autentificação:\n" +
                        "http://" + this.ipBackEnd + ":8080/autentificacao" + "/reenviar/" + id + "\n\n" +
                        "Atenciosamente, equipe uFat"

        );

        try {
            disparadorEmails.send(mensagem);
            return stringRandomica;
        } catch (Exception e) {
            return null;
        }
    }

    @RequestMapping("/reenviar/{id}")
    public void reenviarAutentificacao(@PathVariable Integer id) {

        Optional<Usuario> busca = this.usuarioRepository.findById(id);

        if (!busca.isPresent())
            return;

        Usuario user = busca.get();

        String autentificacao = enviarAutentificacao(user.getIdusuario(), user.getNome(), user.getEmail());

        if (autentificacao == null)
            return;

        user.setAutentificacao(autentificacao);

        this.usuarioRepository.save(user);
    }

    @RequestMapping("/autentificar/usuario/{id}/{codigo}")
    public ResponseEntity<?> autentificar(@PathVariable Integer id, @PathVariable String codigo) {

        Optional<Usuario> busca = this.usuarioRepository.findById(id);

        if (!busca.isPresent())
            ResponseEntity.notFound().build();

        Usuario user = busca.get();

        if (!user.getAutentificacao().equals(codigo))
            ResponseEntity.badRequest().body("Código de autentificação invalido");

        user.setAtivado(0);
        user.setAutentificacao(null);

        user = this.usuarioRepository.save(user);
        if (user.getAtivado() == 0)
            return ResponseEntity.ok("Usuário autentificado com sucesso");
        else
            return ResponseEntity.badRequest().body("Falha ao autentificar o usuário");
    }

}
