package com.backendMobile.backendMobile.Controllers;

import com.backendMobile.backendMobile.Entities.Usuario;
import com.backendMobile.backendMobile.Entities.UsuarioRepository;
import com.backendMobile.backendMobile.POJO.RespostaLogin;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    private final UsuarioRepository usuarios;

    @Autowired
    private EmailController email;

    public UsuarioController(UsuarioRepository usuarios) {
        this.usuarios = usuarios;
    }

    /**
     * Função REST que retorna tda a lista de usuários
     *
     * @return retorna uma lista de usuários ou not found
     */
    @GetMapping
    public ResponseEntity<List<Usuario>> getUsuarios() {
        List<Usuario> listaUsuarios = this.usuarios.findAll();

        if (listaUsuarios == null || (listaUsuarios != null && listaUsuarios.size() == 0)) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(listaUsuarios);
    }

    /**
     * Retorna um usuário especifico
     *
     * @param id identificador do usuário
     * @return retorna o usuário ou not found
     */
    @GetMapping("/{id}")
    public ResponseEntity<Usuario> getUsuarios(@PathVariable Integer id) {
        Usuario usuario = this.usuarios.getOne(id);

        if (usuario == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(usuario);
    }

    /**
     * Adiciona um novo usuário ao banco de dados
     *
     * @param user novo usuário
     * @return retorno o novo usuário salvo
     */
    @PostMapping
    public ResponseEntity<RespostaLogin> addUsuario(@Valid @RequestBody Usuario user) {
        RespostaLogin respostaLogin = new RespostaLogin();
        user.setDataHoraCadastro(new Date());
        user.setAtivado(1);
        try {
            Usuario userSalvo = this.usuarios.save(user);
            if (userSalvo != null) {
                String codigo = this.email.enviarAutentificacao(userSalvo.getIdusuario(), userSalvo.getNome(), userSalvo.getEmail());
                if (codigo != null) {
                    userSalvo.setAutentificacao(codigo);
                    userSalvo = this.usuarios.save(userSalvo);

                    respostaLogin.setUser(userSalvo);
                    respostaLogin.setFlag(true);
                    respostaLogin.setMensagem("OK");
                } else {
                    this.usuarios.delete(userSalvo);
                    respostaLogin.setFlag(false);
                    respostaLogin.setMensagem("Problemas");
                }
            } else {
                respostaLogin.setFlag(false);
                respostaLogin.setMensagem("Problemas");
            }
        } catch (Exception e) {
            respostaLogin.setFlag(false);
            respostaLogin.setMensagem("Problemas");
        }

        return ResponseEntity.ok(respostaLogin);
    }

    /**
     * Altera um usuário já existente
     *
     * @param id   identifdicador do usário que se deseja alterar
     * @param user novas informações do usuário
     * @return retorno o usuário atualizado
     */
    @PutMapping("/{id}")
    public ResponseEntity<Usuario> setUsuario(@PathVariable Integer id, @Valid @RequestBody Usuario user) {
        Usuario existente = this.usuarios.getOne(id);

        if (existente == null) {
            return ResponseEntity.notFound().build();
        }

        BeanUtils.copyProperties(user, existente, "id");

        existente = this.usuarios.save(existente);

        return ResponseEntity.ok(existente);
    }

    @PostMapping("/login")
    public ResponseEntity<RespostaLogin> getUsuarios(@RequestBody Map<String, Object> dados) {
        System.out.println("Entrou no /login");
        RespostaLogin respostaLogin = new RespostaLogin();
        String usuario = (String) dados.get("user");
        String senha = (String) dados.get("senha");
        try {
            Usuario user = this.usuarios.logar(usuario, senha);
            if (user != null) {
                respostaLogin.setFlag(true);
                respostaLogin.setMensagem("Sucesso");
                respostaLogin.setUser(user);
            } else {
                respostaLogin.setFlag(false);
                respostaLogin.setMensagem("Problemas");
            }
        } catch (Exception e) {
            respostaLogin.setFlag(false);
            respostaLogin.setMensagem("Problemas");
        }

        return ResponseEntity.ok(respostaLogin);
    }

}
