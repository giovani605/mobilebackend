package com.backendMobile.backendMobile.Controllers;

import com.backendMobile.backendMobile.POJO.RespostaPadraoPOJO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.Base64;
import java.util.Map;

@RestController
@RequestMapping("/imagem")
public class ImagemController {
//	private final StorageService storageService;
//
//	@Autowired
//	public ImagemController(StorageService storageService) {
//		this.storageService = storageService;
//	}

    @RequestMapping(value = "/{path}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable("path") String imagem) throws IOException {
        System.out.println("pasei");
        File imgFile = new File("./images/" + imagem);
        FileInputStream inputStream = new FileInputStream(imgFile);
        byte[] bytes = StreamUtils.copyToByteArray(inputStream);
        return new ResponseEntity<>(bytes, HttpStatus.OK);

    }

    @PostMapping("/upload")
    public RespostaPadraoPOJO uploadImage(@RequestBody Map<String, String> body) {
        RespostaPadraoPOJO resposta = new RespostaPadraoPOJO();
        try {
            String dados = body.get("imagem");
            byte[] arrayBytes = Base64.getDecoder().decode(dados);
            File f = new File("./images/nova");
            OutputStream stream = new FileOutputStream(f);
            stream.write(arrayBytes);

            resposta.setFlag(true);
            resposta.setMensagem("Sucesso");

        } catch (Exception e) {
            resposta.setFlag(false);
            resposta.setMensagem("Erro");
        }

        return resposta;

    }

}